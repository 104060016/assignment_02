
// calculate phaser's x, y from phaser's theta
function cal_rx(r, theta){
  return - Math.cos( theta / 180 * Math.PI ) * r;
}
function cal_ry(r, theta){
  return - Math.sin( theta / 180 * Math.PI ) * r;
}