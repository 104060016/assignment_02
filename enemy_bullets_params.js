

var enemy_bullets = [
  // 0
  [
    { v: [
      { count:    0, r: 500, theta:  -0, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  -0, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  -4, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  -4, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  -8, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  -8, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -12, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -12, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -16, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -16, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -20, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -20, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -24, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -24, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -28, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -28, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -32, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -32, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -36, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -36, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -40, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -40, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -44, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -44, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -48, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -48, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -52, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -52, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -56, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -56, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -60, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -60, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -64, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -64, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -68, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -68, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -72, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -72, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -76, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -76, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta: -80, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta: -80, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  80, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  80, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  76, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  76, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  72, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  72, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  68, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  68, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  64, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  64, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  60, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  60, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  56, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  56, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  52, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  52, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  48, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  48, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  44, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  44, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  40, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  40, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  36, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  36, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  32, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  32, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  28, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  28, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  24, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  24, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  20, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  20, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  16, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  16, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:  12, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:  12, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:   8, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:   8, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 500, theta:   4, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:   70, r:  50, theta:   4, ra:   0, w: 0.2, raa:   0, wa:   0 }
    ] }
  ],

  // 1
  [
    { v: [
      { count:    0, r: 200, theta:   0, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:   30, r: "i", theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 200, theta: -10, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:   30, r: "i", theta: -10, ra:   0, w:   0, raa:   0, wa:   0 }
    ] },
    { v: [
      { count:    0, r: 200, theta:  10, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:   30, r: "i", theta:  10, ra:   0, w:   0, raa:   0, wa:   0 }
    ] }
  ],

  // 2
  [
    { v: [
      { count:    0, r: 150, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
      ] }
  ],

  // 3
  [
    { v: [
      { count:    0, r: 110, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
      ] },
    { v: [
      { count:    0, r: 110, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 }
      ] },
    { v: [
      { count:    0, r: 110, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 }
      ] }
  ],

  // 4
  [
    { v: [
      { count:    0, r: 120, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 120, theta:  15, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 120, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 120, theta: -15, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 120, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
  ],

  // 5
  [
    { v: [
      { count:    0, r: 350, theta:   0, ra:  -8, w:   0, raa: 0.2, wa:   0 },
      { count:   40, r: "i", theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
  ],

  // 6 -> 圓周6顆
  [
    { v:[
      { count:    0, r: 300, theta:   0, ra: -10, w:   0, raa: 0.2, wa:   0 },
      { count:   40, r: "i", theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v:[
      { count:    0, r: 300, theta:  60, ra: -10, w:   0, raa: 0.2, wa:   0 },
      { count:   40, r: "i", theta:  60, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v:[
      { count:    0, r: 300, theta: 120, ra: -10, w:   0, raa: 0.2, wa:   0 },
      { count:   40, r: "i", theta: 120, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v:[
      { count:    0, r: 300, theta: 180, ra: -10, w:   0, raa: 0.2, wa:   0 },
      { count:   40, r: "i", theta: 180, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v:[
      { count:    0, r: 300, theta: 240, ra: -10, w:   0, raa: 0.2, wa:   0 },
      { count:   40, r: "i", theta: 240, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v:[
      { count:    0, r: 300, theta: 300, ra: -10, w:   0, raa: 0.2, wa:   0 },
      { count:   40, r: "i", theta: 300, ra:   0, w:   0, raa:   0, wa:   0 }
    ]}
  ],

  // 7 -> 等速彈
  [
    { v: [
      { count:    0, r: 175, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]}
  ],

  // 8 -> 驟減
  [
    { v: [
      { count:    0, r: 800, theta:   0, ra: -26, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r: "i", theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 800, theta:  10, ra: -26, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r: "i", theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]}
  ],

  // 9 ->  eirin
  [
    // 9-1 -> 直線彈
    { v: [
      { count:    0, r:  30, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   30, r: 240, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r:  60, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   30, r: 240, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r:  90, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   30, r: 240, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 120, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   30, r: 240, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 150, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   30, r: 240, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 180, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   30, r: 240, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 210, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   30, r: 240, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 240, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   30, r: 240, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    // 9-2 -> 暫停加速
    { v: [
      { count:    0, r: 280, theta:   0, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta:   0, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta:   0, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta:  10, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta:  10, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta:  10, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta:  20, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta:  20, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta:  20, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta:  30, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta:  30, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta:  30, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta:  40, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta:  40, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta:  40, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta:  50, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta:  50, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta:  50, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta: -10, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta: -10, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta: -10, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta: -20, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta: -20, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta: -20, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta: -30, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta: -30, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta: -30, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta: -40, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta: -40, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta: -40, ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:    0, r: 280, theta: -50, ra: -11, w:   0, raa: 0.1, wa:   0 },
      { count:   30, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:   80, r:   0, theta: -50, ra:   5, w:   0, raa:   0, wa:   0 },
      { count:  150, r: "i", theta: -50, ra:   0, w:   0, raa:   0, wa:   0 }
    ]}
  ],

  // 10 -> 0 修改
  [
    { v: [
      { count:     0, r: 400, theta:   0, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:   0, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  10, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  10, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  20, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  20, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  30, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  30, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  40, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  40, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  50, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  50, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  60, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  60, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  70, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  70, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  80, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  80, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  90, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  90, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 100, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 100, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 110, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 110, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 120, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 120, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 130, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 130, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 140, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 140, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 150, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 150, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 160, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 160, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 170, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 170, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 180, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 180, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 190, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 190, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 200, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 200, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 210, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 210, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 220, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 220, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 230, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 230, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 240, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 240, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 250, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 250, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 260, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 260, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 270, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 270, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 280, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 280, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 290, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 290, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 300, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 300, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 310, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 310, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 320, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 320, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 330, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 330, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 340, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 340, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 350, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 350, ra:   0, w: 0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
  ],

  // 11 -> 旋轉圓
  [
    { v: [
      { count:     0, r: 400, theta:   0, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:   0, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  10, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  10, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  20, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  20, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  30, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  30, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  40, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  40, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  50, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  50, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  60, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  60, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  70, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  70, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  80, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  80, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta:  90, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta:  90, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 100, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 100, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 110, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 110, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 120, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 120, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 130, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 130, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 140, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 140, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 150, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 150, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 160, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 160, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 170, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 170, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 180, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 180, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 190, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 190, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 200, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 200, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 210, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 210, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 220, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 220, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 230, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 230, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 240, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 240, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 250, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 250, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 260, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 260, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 270, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 270, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 280, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 280, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 290, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 290, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 300, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 300, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 310, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 310, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 320, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 320, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 330, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 330, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 340, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 340, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 400, theta: 350, ra: -10, w:   0, raa: 0.1, wa:   0 },
      { count:    40, r:  50, theta: 350, ra:   0, w:-0.2, raa:   0, wa:   0 },
      { count:   240, r:  50, theta: "i", ra:   0, w:   0, raa:   0, wa:   0 }
    ]},
  ],

  // 12 -> 90度扇(向左下)
  [
    { v: [
      { count:     0, r: 100, theta:   0, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:  10, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:  20, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:  30, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:  40, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:  50, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:  60, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:  70, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:  80, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:  90, ra:   0, w:   0, raa:    0, wa:   0 }
    ]}
  ],

  // 13 -> 90度扇(向左上)
  [
    { v: [
      { count:     0, r: 100, theta:  90, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: 100, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: 110, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: 120, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: 130, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: 140, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: 150, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: 160, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: 170, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: 180, ra:   0, w:   0, raa:    0, wa:   0 }
    ]}
  ],

  // 14 -> 90度扇(向右上)
  [
    { v: [
      { count:     0, r: 100, theta: -90, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:-100, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:-110, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:-120, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:-130, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:-140, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:-150, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:-160, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:-170, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta:-180, ra:   0, w:   0, raa:    0, wa:   0 }
    ]}
  ],
  
  // 15 -> 90度扇(向右下)
  [
    { v: [
      { count:     0, r: 100, theta: - 0, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: -10, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: -20, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: -30, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: -40, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: -50, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: -60, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: -70, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: -80, ra:   0, w:   0, raa:    0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 100, theta: -90, ra:   0, w:   0, raa:    0, wa:   0 }
    ]}
  ],

  
  // 16 -> 11旋轉圓 修改
  [
    // theta +- 0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165, 180
    { v: [
      { count:     0, r: 215, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:   0, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:   0, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:   0, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:   0, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:   0, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:   0, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:   0, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:   0, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  10, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  10, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  10, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  10, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  10, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  10, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  10, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  10, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  20, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  20, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  20, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  20, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  20, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  20, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  20, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  20, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  30, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  30, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  30, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  30, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  30, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  30, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  30, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  30, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  40, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  40, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  40, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  40, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  40, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  40, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  40, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  40, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  50, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  50, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  50, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  50, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  50, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  50, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  50, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  50, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  60, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  60, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  60, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  60, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  60, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  60, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  60, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  60, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  70, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  70, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  70, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  70, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  70, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  70, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  70, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  70, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  80, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  80, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  80, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  80, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  80, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  80, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  80, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  80, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  90, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  90, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  90, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  90, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  90, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  90, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  90, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  90, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 100, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 100, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 100, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 100, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 100, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 100, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 100, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 100, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 110, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 110, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 110, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 110, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 110, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 110, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 110, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 110, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 120, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 120, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 120, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 120, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 120, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 120, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 120, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 120, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 130, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 130, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 130, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 130, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 130, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 130, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 130, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 130, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 140, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 140, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 140, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 140, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 140, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 140, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 140, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 140, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 150, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 150, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 150, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 150, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 150, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 150, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 150, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 150, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 160, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 160, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 160, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 160, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 160, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 160, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 160, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 160, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 170, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 170, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 170, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 170, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 170, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 170, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 170, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 170, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 180, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 180, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 180, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 180, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 180, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 180, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 180, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 180, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -10, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -10, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -10, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -10, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -10, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -10, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -10, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -10, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -20, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -20, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -20, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -20, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -20, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -20, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -20, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -20, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -30, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -30, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -30, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -30, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -30, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -30, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -30, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -30, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -40, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -40, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -40, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -40, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -40, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -40, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -40, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -40, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -50, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -50, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -50, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -50, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -50, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -50, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -50, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -50, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -60, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -60, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -60, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -60, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -60, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -60, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -60, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -60, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -70, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -70, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -70, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -70, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -70, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -70, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -70, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -70, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -80, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -80, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -80, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -80, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -80, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -80, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -80, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -80, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -90, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -90, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -90, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -90, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -90, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -90, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -90, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -90, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-100, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-100, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-100, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-100, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-100, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-100, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-100, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-100, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-110, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-110, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-110, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-110, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-110, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-110, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-110, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-110, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-120, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-120, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-120, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-120, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-120, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-120, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-120, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-120, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-130, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-130, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-130, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-130, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-130, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-130, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-130, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-130, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-140, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-140, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-140, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-140, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-140, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-140, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-140, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-140, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-150, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-150, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-150, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-150, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-150, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-150, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-150, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-150, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-160, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-160, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-160, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-160, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-160, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-160, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-160, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-160, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-170, ra:1.34, w: 0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-170, ra:1.32, w: 0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-170, ra:1.30, w: 0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-170, ra:1.28, w: 0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-170, ra:1.26, w: 0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-170, ra:1.24, w: 0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-170, ra:1.22, w: 0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-170, ra:1.20, w: 0.4, raa:   0, wa:   0 }
    ]}
  ],








  
  // 17 -> 16逆
  [
    // theta +- 0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165, 180
    { v: [
      { count:     0, r: 215, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:   0, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:   0, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:   0, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:   0, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:   0, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:   0, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:   0, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:   0, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:   0, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:   0, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  10, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  10, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  10, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  10, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  10, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  10, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  10, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  10, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  10, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  20, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  20, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  20, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  20, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  20, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  20, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  20, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  20, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  20, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  30, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  30, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  30, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  30, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  30, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  30, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  30, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  30, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  30, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  40, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  40, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  40, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  40, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  40, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  40, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  40, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  40, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  40, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  50, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  50, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  50, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  50, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  50, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  50, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  50, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  50, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  50, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  60, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  60, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  60, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  60, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  60, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  60, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  60, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  60, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  60, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  70, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  70, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  70, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  70, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  70, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  70, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  70, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  70, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  70, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  80, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  80, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  80, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  80, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  80, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  80, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  80, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  80, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  80, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:  90, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:  90, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:  90, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:  90, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:  90, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:  90, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:  90, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:  90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:  90, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:  90, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 100, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 100, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 100, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 100, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 100, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 100, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 100, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 100, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 100, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 110, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 110, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 110, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 110, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 110, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 110, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 110, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 110, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 110, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 120, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 120, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 120, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 120, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 120, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 120, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 120, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 120, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 120, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 130, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 130, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 130, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 130, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 130, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 130, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 130, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 130, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 130, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 140, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 140, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 140, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 140, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 140, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 140, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 140, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 140, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 140, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 150, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 150, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 150, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 150, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 150, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 150, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 150, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 150, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 150, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 160, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 160, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 160, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 160, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 160, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 160, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 160, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 160, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 160, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 170, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 170, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 170, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 170, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 170, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 170, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 170, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 170, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 170, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: 180, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: 180, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: 180, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: 180, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: 180, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: 180, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: 180, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: 180, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: 180, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: 180, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -10, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -10, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -10, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -10, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -10, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -10, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -10, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -10, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -10, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -10, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -20, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -20, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -20, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -20, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -20, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -20, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -20, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -20, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -20, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -20, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -30, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -30, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -30, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -30, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -30, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -30, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -30, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -30, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -30, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -30, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -40, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -40, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -40, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -40, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -40, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -40, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -40, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -40, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -40, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -40, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -50, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -50, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -50, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -50, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -50, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -50, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -50, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -50, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -50, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -50, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -60, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -60, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -60, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -60, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -60, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -60, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -60, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -60, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -60, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -60, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -70, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -70, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -70, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -70, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -70, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -70, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -70, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -70, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -70, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -70, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -80, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -80, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -80, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -80, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -80, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -80, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -80, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -80, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -80, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -80, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta: -90, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta: -90, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta: -90, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta: -90, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta: -90, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta: -90, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta: -90, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta: -90, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta: -90, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta: -90, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-100, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-100, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-100, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-100, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-100, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-100, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-100, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-100, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-100, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-100, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-110, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-110, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-110, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-110, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-110, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-110, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-110, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-110, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-110, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-110, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-120, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-120, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-120, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-120, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-120, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-120, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-120, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-120, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-120, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-120, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-130, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-130, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-130, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-130, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-130, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-130, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-130, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-130, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-130, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-130, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-140, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-140, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-140, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-140, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-140, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-140, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-140, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-140, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-140, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-140, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-150, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-150, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-150, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-150, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-150, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-150, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-150, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-150, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-150, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-150, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-160, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-160, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-160, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-160, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-160, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-160, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-160, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-160, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-160, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-160, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]},

    { v: [
      { count:     0, r: 215, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.14, w:   0, raa:   0, wa:   0 },
      { count:    90, r: "i", theta:-170, ra:1.34, w:-0.47, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 210, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.12, w:   0, raa:   0, wa:   0 },
      { count:    95, r: "i", theta:-170, ra:1.32, w:-0.46, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 205, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.10, w:   0, raa:   0, wa:   0 },
      { count:   100, r: "i", theta:-170, ra:1.30, w:-0.45, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 200, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.08, w:   0, raa:   0, wa:   0 },
      { count:   105, r: "i", theta:-170, ra:1.28, w:-0.44, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 195, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.06, w:   0, raa:   0, wa:   0 },
      { count:   110, r: "i", theta:-170, ra:1.26, w:-0.43, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 190, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.04, w:   0, raa:   0, wa:   0 },
      { count:   115, r: "i", theta:-170, ra:1.24, w:-0.42, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 185, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.02, w:   0, raa:   0, wa:   0 },
      { count:   120, r: "i", theta:-170, ra:1.22, w:-0.41, raa:   0, wa:   0 }
    ]},
    { v: [
      { count:     0, r: 180, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    35, r:   0, theta:-170, ra:   0, w:   0, raa:   0, wa:   0 },
      { count:    60, r:   0, theta:-170, ra:1.00, w:   0, raa:   0, wa:   0 },
      { count:   125, r: "i", theta:-170, ra:1.20, w:-0.4, raa:   0, wa:   0 }
    ]}
  ]
]