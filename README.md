# Assignment_02

## 操作簡介
#### menu
- ↑↓, space
- space開始遊戲

#### stagestate
- 移動
  - ↑↓←→
- 射擊
  - space
  

## Score
|Component|Score|Done|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|N|
|Appearance(subjective)|5%|N|
